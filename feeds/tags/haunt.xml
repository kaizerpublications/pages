<?xml version="1.0" encoding="utf-8"?><feed xmlns="http://www.w3.org/2005/Atom"><title>Kaizer Publications</title><id>https://kaizerpublications.xyz/feeds/tags/haunt.xml</id><subtitle>Tag: haunt</subtitle><updated>2024-11-15T10:56:18Z</updated><link href="https://kaizerpublications.xyz/feeds/tags/haunt.xml" rel="self" /><link href="https://kaizerpublications.xyz" /><entry><title>Haunt - Create or Die</title><id>https://kaizerpublications.xyz/haunt---create-or-die.html</id><author><name>Kaizer</name><email>kaizer@disroot.org</email></author><updated>2024-07-13T00:00:00Z</updated><link href="https://kaizerpublications.xyz/haunt---create-or-die.html" rel="alternate" /><content type="html">&lt;p&gt;
Kaizer Publications was built using &lt;a href=&quot;https://dthompson.us/projects/haunt.html&quot;&gt;haunt&lt;/a&gt;. Haunt is a FOSS static site generator written in &lt;a href=&quot;https://www.gnu.org/software/guile&quot;&gt;Guile Scheme&lt;/a&gt;. By the end of this post, hopefully, you will see why.
&lt;/p&gt;&lt;div id=&quot;outline-container-org6009941&quot; class=&quot;outline-2&quot;&gt;
&lt;h2 id=&quot;org6009941&quot;&gt;Why FOSS?&lt;/h2&gt;
&lt;div id=&quot;text-org6009941&quot; class=&quot;outline-text-2&quot;&gt;
&lt;p&gt;
More often than not, I get the question as to why Free and Open Source Software is important. There are many reasons to use FOSS. In my understanding, it all boils down to the fact that you can't fake it. Everything is open and eveybody can work on what they want. If you believe your community knows what's up the software will prove it. If you believe in something but you're not willing to sacrifice for it, the software will show it. In other words, truth is self-evident. You either got it or you don't all else is vanity.
&lt;/p&gt;

&lt;p&gt;
But it doesn't stop there. We are human beings with the ability to learn. Everyday we work on building our lives in the direction of our dreams and we will accept nothing less than complete victory. Regardless of whether or not others believe in our dreams, we believe. This is who we are. We only have one life and what a waste it would be to spend it chasing other people's dreams when we have our own.
&lt;/p&gt;
&lt;/div&gt;
&lt;/div&gt;&lt;div id=&quot;outline-container-org4b070cb&quot; class=&quot;outline-2&quot;&gt;
&lt;h2 id=&quot;org4b070cb&quot;&gt;Why Guile?&lt;/h2&gt;
&lt;div id=&quot;text-org4b070cb&quot; class=&quot;outline-text-2&quot;&gt;
&lt;p&gt;
Now that we've gotten the heavy stuff out of the way, we need to talk about why guile is in my opinion the best programming language ever. Guile is an implementation of scheme from the &lt;a href=&quot;https://gnu.org&quot;&gt;GNU foundation&lt;/a&gt;. However I choose to describe this language somebody can probably find a mistake as I am not an expert but I can tell you what I love about it.
&lt;/p&gt;

&lt;p&gt;
Scheme is a programming language based on the lambda calculus (which, by the way I am now fervently trying to learn, just 23 mathematical textbooks to go to complete my intro to programming, more on this in a later post). What this means to me, is that you can use mathematics like a paint brush to create amazing worlds. An example is recursion. Recursion is an artistic way of playing around with stack frames based on mathematical principles. Think &lt;a href=&quot;https://en.wikipedia.org/wiki/Tower_of_Hanoi&quot;&gt;tower of hanoi&lt;/a&gt; on steroids. And yes, that's just touching the surface, we haven't gotten to AI just yet.
&lt;/p&gt;

&lt;p&gt;
Therefore, what I am trying to say is that to me Guile is a very artistic language that allows you to express yourself and your ideas as best as you can. For example, when I am writing a post to this blog, I just write a post on my pc and update the git repo. Done. No signups, no ads. I can use my beloved emacs to the max. Guile allows me to do all that and create that system to my liking.
&lt;/p&gt;
&lt;/div&gt;
&lt;/div&gt;&lt;div id=&quot;outline-container-orga7d93da&quot; class=&quot;outline-2&quot;&gt;
&lt;h2 id=&quot;orga7d93da&quot;&gt;Why Haunt?&lt;/h2&gt;
&lt;div id=&quot;text-orga7d93da&quot; class=&quot;outline-text-2&quot;&gt;
&lt;p&gt;
Haunt is a static site generator written in Guile. So it provides me with all the aforementioned benefits of Guile, but in an amazing package. It takes away all the pain associated with using a static site generator. I can create templates, I can insert interactive wasm apps, I can throw in whatever mix of programming languages I need. I've seen it been used to create comment sections (which I won't do, just send me an email, this isn't big tech). I've seen it been used to create authentication for accounts (which might be coming depending on how my marketplace implementation goes). Haunt is super flexible, limited only by your creativity. I could talk more about Haunt or I could let this blog speak for itself. I choose to let the art speak for itself.
&lt;/p&gt;
&lt;/div&gt;
&lt;/div&gt;&lt;div id=&quot;outline-container-org3c3ec34&quot; class=&quot;outline-2&quot;&gt;
&lt;h2 id=&quot;org3c3ec34&quot;&gt;Why Create or Die?&lt;/h2&gt;
&lt;div id=&quot;text-org3c3ec34&quot; class=&quot;outline-text-2&quot;&gt;
&lt;p&gt;
Now the question becomes why go this hard? Like what is there to be achieved? The answer is probably nothing. There are no awards, no ceremonies, no prizes, no songs, no buxom beauties, no glory. So why? This just sounds like a lot of hard work. For me, the answer is simple. I am here to create beauty. That's what I want. It's like why you give your girl roses when you see her. Those dead roses can't compare to her radiant smile. That's not the point. That was never the point. It's deeper than that. Why are you here? For glory? For awards? Or to live? What kind of life do you want to live?
&lt;/p&gt;

&lt;p&gt;
Just like in the myth of Sisyphus, just before the boulder comes rolling back down, in that moment Sisyphus has triumphed. And that's all that matters to Sisyphus. The difference is that you are not Sisyphus, and you are not condemned by the gods to eternal torture. You have your whole life ahead of you and you are inescapably free. What willl you do with your freedom? Create beauty or die?
&lt;/p&gt;
&lt;/div&gt;
&lt;/div&gt;</content></entry></feed>